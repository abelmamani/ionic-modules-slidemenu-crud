import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'bus',
    pathMatch: 'full'
  },
  {
    path: 'conductor',
    loadChildren: () => import('./modules/conductor/conductor.module').then( m => m.ConductorPageModule)
  },
  {
    path: 'bus',
    loadChildren: () => import('./modules/bus/bus.module').then( m => m.BusPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
