import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Buses', url: '/bus', icon: 'mail' },
    { title: 'Conductors', url: '/conductor', icon: 'paper-plane' },
  
  ];
  constructor() {}
}
