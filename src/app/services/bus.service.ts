import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bus } from '../models/bus.model';

@Injectable({
  providedIn: 'root'
})
export class BusService {
  url: string = 'http://localhost:8080/bus';
  constructor(private http: HttpClient) {}

  getBuses(): Observable<Bus[]>{
    return this.http.get<Bus[]>(this.url);
  }
  getBusById(id: number): Observable<Bus>{
    return this.http.get<Bus>(this.url+'/'+id);
  }
  addBus(bus: Bus): Observable<any>{
    return this.http.post(this.url, bus);
  }
  updateBus(bus: Bus): Observable<any>{
    return this.http.put(this.url, bus);
  }
  deleteBus(id: number): Observable<any>{
    return this.http.delete(this.url+'/'+id);
  }

}
