import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Bus } from 'src/app/models/bus.model';
import { BusService } from 'src/app/services/bus.service';
import { AddBusModalComponent } from './components/add-bus-modal/add-bus-modal.component';
import { UpdateBusModalComponent } from './components/update-bus-modal/update-bus-modal.component';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.page.html',
  styleUrls: ['./bus.page.scss'],
})
export class BusPage implements OnInit {
  buses: Bus[] = [];
  id: number = 0;
  isAlertOpen: boolean = false;
  isToastOpen: boolean = false;
  message: string = '';
  constructor(private busService: BusService, private modalCtrl: ModalController) { }
  ngOnInit() {
    this.getAllBuses();
  }
  
  getAllBuses(){
    this.busService.getBuses().subscribe({
      next: (resp: Bus[]) => {
        this.buses = resp;
      },
      error: (error) => {
        this.message = 'ocurrio un error al cargar listado';
        this.setOpen(true);
      }
    }); 
  }

  async openAddModal() {
    const modal = await this.modalCtrl.create({component: AddBusModalComponent,});
    modal.present();
    const { role } = await modal.onWillDismiss();
    if (role === 'confirm') {
      this.message = "Se agrego nuevo bus!";
      this.setOpen(true);
      this.getAllBuses();
    }
  }

  async openUpdateModal(id: number) {
    const modal = await this.modalCtrl.create({
      component: UpdateBusModalComponent, componentProps: {idBus: id }});
    modal.present();
    const { role } = await modal.onWillDismiss();
    if (role === 'confirm') {
      this.message = "Se actualizo exitosamente!";
      this.setOpen(true);
      this.getAllBuses();
    }
  }
  
  deleteBus(id: number){
    this.setOpenAlert(true);
    this.id = id;
  }

  setOpenAlert(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
  setOpen(isOpen: boolean) {
    this.isToastOpen = isOpen;
  }

  public alertButtons = [
    {
      text: 'No',
      role: 'cancel',
      handler: () => {
        this.message = 'Accion cancelada!';
        this.setOpen(true);
      },
    },
    {
      text: 'Si',
      role: 'confirm',
      handler: () => {
        this.busService.deleteBus(this.id).subscribe({
          next: (res) => {
            this.message = 'Se elimino bus con id ' + this.id;
            this.setOpen(true);
            this.getAllBuses();
          }, 
          error: (error) =>{
            this.message = 'ocurrio un error al eliminar!';
            this.setOpen(true);
          }
        });
      },
    },
  ];
}
