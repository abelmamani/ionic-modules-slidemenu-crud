import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { BusPageRoutingModule } from './bus-routing.module';
import { BusPage } from './bus.page';
import { AddBusModalComponent } from './components/add-bus-modal/add-bus-modal.component';
import { UpdateBusModalComponent } from './components/update-bus-modal/update-bus-modal.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    BusPageRoutingModule
  ],
  declarations: [BusPage, AddBusModalComponent, UpdateBusModalComponent]
})
export class BusPageModule {}
