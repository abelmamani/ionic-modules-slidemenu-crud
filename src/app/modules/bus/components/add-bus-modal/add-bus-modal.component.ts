import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-add-bus-modal',
  templateUrl: './add-bus-modal.component.html',
  styleUrls: ['./add-bus-modal.component.scss'],
})
export class AddBusModalComponent  implements OnInit {
  formBus!: FormGroup;
  isToastOpen: boolean = false;
  message: string = "";

  constructor(private modalCtrl: ModalController, private busService: BusService, private formBuilder: FormBuilder) {
    this.formBus = this.formBuilder.group({
      id: 0,
      licensePlate: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45)]],
      brand: ['', Validators.required],
      model: ['', Validators.required],
      seats: [null, Validators.required]
    });
  }
  
  ngOnInit() {}
  
  formSubmit(){
    if(this.formBus.valid){
      this.busService.addBus(this.formBus.value).subscribe({
        next: (response) => {
          this.confirm();
        },
        error: (error) => {
          if(error.status == 0){
            this.message = "error, fallo la conexion!";
          }else{
            this.message = error.error.message;
          }
          this.setOpen(true);
        }
      });
    }
  }
  
  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(null, 'confirm');
  }
  setOpen(state: boolean){
    this.isToastOpen = state;
  }

}
