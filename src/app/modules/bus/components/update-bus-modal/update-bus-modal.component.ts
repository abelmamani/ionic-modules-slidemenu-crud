import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Bus } from 'src/app/models/bus.model';
import { BusService } from 'src/app/services/bus.service';

@Component({
  selector: 'app-update-bus-modal',
  templateUrl: './update-bus-modal.component.html',
  styleUrls: ['./update-bus-modal.component.scss'],
})
export class UpdateBusModalComponent  implements OnInit {
  @Input() idBus!: number;
  formBus!: FormGroup;
  isToastOpen: boolean = false;
  message: string = "";

  constructor(private modalCtrl: ModalController, private busService: BusService, private formBuilder: FormBuilder) {
    this.formBus = this.formBuilder.group({
      id: 0,
      licensePlate: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(45)]],
      brand: ['', Validators.required],
      model: ['', Validators.required],
      seats: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.busService.getBusById(this.idBus).subscribe({
      next: (res: Bus) =>{
        this.formBus.patchValue(res);
      }, 
      error: (error) => {
        this.message = "Ocurrio un error al obtener el bus.";
        this.setOpen(true);
      }
    });
  }

  formSubmit(){
    if(this.formBus.valid){
      this.busService.updateBus(this.formBus.value).subscribe({
        next: (res) => {
          this.confirm();
        }, 
        error: (error) => {
          if(error.status == 0){
            this.message = "error, fallo la conexion!";
          }else{
            this.message = error.error.message;
          }
          this.setOpen(true);
        }
      });
    }
  }
  
  cancel() {
    return this.modalCtrl.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalCtrl.dismiss(null, 'confirm');
  }

  setOpen(state: boolean){
    this.isToastOpen = state;
  }
}
